-- phpMyAdmin SQL Dump
-- version 5.1.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Mar 30, 2021 at 04:39 AM
-- Server version: 5.7.33-0ubuntu0.18.04.1
-- PHP Version: 7.2.24-0ubuntu0.18.04.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cryptocricket`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `id` int(11) NOT NULL,
  `username` varchar(225) NOT NULL,
  `password` varchar(225) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`id`, `username`, `password`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `Matches`
--

CREATE TABLE `Matches` (
  `id` int(11) NOT NULL,
  `cricket_api_match_key` varchar(255) DEFAULT NULL,
  `match_name` varchar(255) DEFAULT NULL,
  `match_short_name` varchar(255) DEFAULT NULL,
  `match_title` varchar(255) DEFAULT NULL,
  `match_season` varchar(255) DEFAULT NULL,
  `match_formate` varchar(255) DEFAULT NULL,
  `match_venue` varchar(255) DEFAULT NULL,
  `team_one` varchar(125) DEFAULT NULL,
  `team_two` varchar(125) DEFAULT NULL,
  `match_date_time` datetime DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Matches`
--

INSERT INTO `Matches` (`id`, `cricket_api_match_key`, `match_name`, `match_short_name`, `match_title`, `match_season`, `match_formate`, `match_venue`, `team_one`, `team_two`, `match_date_time`, `createdAt`, `updatedAt`) VALUES
(1, 'afgzim_2021_test_01', 'Afghanistan vs Zimbabwe', 'AFG vs ZIM', 'Afghanistan vs Zimbabwe - 1st Test Match - Afghanistan v Zimbabwe in UAE, 2021', 'Afghanistan v Zimbabwe in UAE, 2021', 'test', NULL, 'Afghanistan', 'Zimbabwe', '2021-03-02 05:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(2, 'pslt20_2021_g12', 'Islamabad United vs Quetta Gladiators', 'IU vs QG', 'Islamabad United vs Quetta Gladiators - 12th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Islamabad United', 'Quetta Gladiators', '2021-03-02 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(3, 'nzwengw_2021_t20_01', 'New Zealand Women vs England Women', 'NZW vs ENGW', 'New Zealand Women vs England Women - 1st T20 Match - New Zealand Women vs England Women', 'New Zealand Women vs England Women', 't20', NULL, 'New Zealand Women', 'England Women', '2021-03-03 02:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(4, 'nzaus_2021_t20_03', 'New Zealand vs Australia', 'NZ vs AUS', 'New Zealand vs Australia - 3rd T20 Match - New Zealand vs Australia 2021', 'New Zealand vs Australia 2021', 't20', NULL, 'New Zealand', 'Australia', '2021-03-03 06:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(5, 'pslt20_2021_g14', 'Quetta Gladiators vs Multan Sultans', 'QG vs MS', 'Quetta Gladiators vs Multan Sultans - 14th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Quetta Gladiators', 'Multan Sultans', '2021-03-03 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(6, 'pslt20_2021_g13', 'Karachi Kings vs Peshawar Zalmi', 'KK vs PZ', 'Karachi Kings vs Peshawar Zalmi - 13th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Karachi Kings', 'Peshawar Zalmi', '2021-03-03 09:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(7, 'wisl_2021_t20_01', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 1st T20 Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 't20', NULL, 'West Indies', 'Sri Lanka', '2021-03-03 22:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(8, 'indeng_2021_test_04', 'India vs England', 'IND vs ENG', 'India vs England - 4th Test Match - India vs England 2021', 'India vs England 2021', 'test', NULL, 'India', 'England', '2021-03-04 04:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(9, 'pslt20_2021_g15', 'Lahore Qalandars vs Islamabad United', 'LQ vs IU', 'Lahore Qalandars vs Islamabad United - 15th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Lahore Qalandars', 'Islamabad United', '2021-03-04 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(10, 'nzwengw_2021_t20_02', 'New Zealand Women vs England Women', 'NZW vs ENGW', 'New Zealand Women vs England Women - 2nd T20 Match - New Zealand Women vs England Women', 'New Zealand Women vs England Women', 't20', NULL, 'New Zealand Women', 'England Women', '2021-03-05 02:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(11, 'nzaus_2021_t20_04', 'New Zealand vs Australia', 'NZ vs AUS', 'New Zealand vs Australia - 4th T20 Match - New Zealand vs Australia 2021', 'New Zealand vs Australia 2021', 't20', NULL, 'New Zealand', 'Australia', '2021-03-05 06:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(12, 'rswst20_2020_g05', 'India Legends vs Bangladesh Legends', 'INDL vs BANL', 'India Legends vs Bangladesh Legends - 5th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'India Legends', 'Bangladesh Legends', '2021-03-05 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(13, 'pslt20_2021_g16', 'Karachi Kings vs Multan Sultans', 'KK vs MS', 'Karachi Kings vs Multan Sultans - 16th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Karachi Kings', 'Multan Sultans', '2021-03-05 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(14, 'wisl_2021_t20_02', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 2nd T20 Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 't20', NULL, 'West Indies', 'Sri Lanka', '2021-03-05 22:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(15, 'pslt20_2021_g17', 'Islamabad United vs Quetta Gladiators', 'IU vs QG', 'Islamabad United vs Quetta Gladiators - 17th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Islamabad United', 'Quetta Gladiators', '2021-03-06 09:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(16, 'rswst20_2020_g06', 'Sri Lanka Legends vs West Indies Legends', 'SLL vs WIL', 'Sri Lanka Legends vs West Indies Legends - 6th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'Sri Lanka Legends', 'West Indies Legends', '2021-03-06 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(17, 'pslt20_2021_g18', 'Peshawar Zalmi vs Lahore Qalandars', 'PZ vs LQ', 'Peshawar Zalmi vs Lahore Qalandars - 18th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Peshawar Zalmi', 'Lahore Qalandars', '2021-03-06 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(18, 'nzaus_2021_t20_05', 'New Zealand vs Australia', 'NZ vs AUS', 'New Zealand vs Australia - 5th T20 Match - New Zealand vs Australia 2021', 'New Zealand vs Australia 2021', 't20', NULL, 'New Zealand', 'Australia', '2021-03-06 23:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(19, 'nzwengw_2021_t20_03', 'New Zealand Women vs England Women', 'NZW vs ENGW', 'New Zealand Women vs England Women - 3rd T20 Match - New Zealand Women vs England Women', 'New Zealand Women vs England Women', 't20', NULL, 'New Zealand Women', 'England Women', '2021-03-07 03:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(20, 'indwrsaw_2021_one-day_01', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 1st ODI Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 'one-day', NULL, 'India Women', 'South Africa Women', '2021-03-07 03:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(21, 'pslt20_2021_g19', 'Multan Sultans vs Quetta Gladiators', 'MS vs QG', 'Multan Sultans vs Quetta Gladiators - 19th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Multan Sultans', 'Quetta Gladiators', '2021-03-07 09:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(22, 'rswst20_2020_g07', 'England Legends vs Bangladesh Legends', 'ENGL vs BANL', 'England Legends vs Bangladesh Legends - 7th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'England Legends', 'Bangladesh Legends', '2021-03-07 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(23, 'pslt20_2021_g20', 'Islamabad United vs Karachi Kings', 'IU vs KK', 'Islamabad United vs Karachi Kings - 20th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Islamabad United', 'Karachi Kings', '2021-03-07 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(24, 'wisl_2021_t20_03', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 3rd T20 Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 't20', NULL, 'West Indies', 'Sri Lanka', '2021-03-07 22:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(25, 'rswst20_2020_g08', 'South Africa Legends vs Sri Lanka Legends', 'RSAL vs SLL', 'South Africa Legends vs Sri Lanka Legends - 8th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'South Africa Legends', 'Sri Lanka Legends', '2021-03-08 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(26, 'indwrsaw_2021_one-day_02', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 2nd ODI Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 'one-day', NULL, 'India Women', 'South Africa Women', '2021-03-09 03:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(27, 'rswst20_2020_g09', 'India Legends vs England Legends', 'INDL vs ENGL', 'India Legends vs England Legends - 9th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'India Legends', 'England Legends', '2021-03-09 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(28, 'afgzim_2021_test_02', 'Afghanistan vs Zimbabwe', 'AFG vs ZIM', 'Afghanistan vs Zimbabwe - 2nd Test Match - Afghanistan v Zimbabwe in UAE, 2021', 'Afghanistan v Zimbabwe in UAE, 2021', 'test', NULL, 'Afghanistan', 'Zimbabwe', '2021-03-10 05:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(29, 'rswst20_2020_g010', 'Bangladesh Legends vs Sri Lanka Legends', 'BANL vs SLL', 'Bangladesh Legends vs Sri Lanka Legends - 10th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'Bangladesh Legends', 'Sri Lanka Legends', '2021-03-10 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(30, 'wisl_2021_one-day_01', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 1st ODI Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 'one-day', NULL, 'West Indies', 'Sri Lanka', '2021-03-10 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(31, 'pslt20_2021_g21', 'Peshawar Zalmi vs Karachi Kings', 'PZ vs KK', 'Peshawar Zalmi vs Karachi Kings - 21st Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Peshawar Zalmi', 'Karachi Kings', '2021-03-10 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(32, 'rswst20_2020_g11', 'England Legends vs South Africa Legends', 'ENGL vs RSAL', 'England Legends vs South Africa Legends - 11th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'England Legends', 'South Africa Legends', '2021-03-11 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(33, 'pslt20_2021_g22', 'Lahore Qalandars vs Quetta Gladiators', 'LQ vs QG', 'Lahore Qalandars vs Quetta Gladiators - 22nd Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Lahore Qalandars', 'Quetta Gladiators', '2021-03-11 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(34, 'indwrsaw_2021_one-day_03', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 3rd ODI Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 'one-day', NULL, 'India Women', 'South Africa Women', '2021-03-12 03:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(35, 'pslt20_2021_g23', 'Multan Sultans vs Peshawar Zalmi', 'MS vs PZ', 'Multan Sultans vs Peshawar Zalmi - 23rd Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Multan Sultans', 'Peshawar Zalmi', '2021-03-12 10:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(36, 'rswst20_2020_g12', 'Bangladesh Legends vs West Indies Legends', 'BANL vs WIL', 'Bangladesh Legends vs West Indies Legends - 12th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'Bangladesh Legends', 'West Indies Legends', '2021-03-12 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(37, 'wisl_2021_one-day_02', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 2nd ODI Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 'one-day', NULL, 'West Indies', 'Sri Lanka', '2021-03-12 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(38, 'indeng_2021_t20_01', 'India vs England', 'IND vs ENG', 'India vs England - 1st T20 Match - India vs England 2021', 'India vs England 2021', 't20', NULL, 'India', 'England', '2021-03-12 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(39, 'pslt20_2021_g24', 'Lahore Qalandars vs Islamabad United', 'LQ vs IU', 'Lahore Qalandars vs Islamabad United - 24th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Lahore Qalandars', 'Islamabad United', '2021-03-12 15:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(40, 'pslt20_2021_g25', 'Quetta Gladiators vs Karachi Kings', 'QG vs KK', 'Quetta Gladiators vs Karachi Kings - 25th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Quetta Gladiators', 'Karachi Kings', '2021-03-13 09:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(41, 'rswst20_2020_g13', 'India Legends vs South Africa Legends', 'INDL vs RSAL', 'India Legends vs South Africa Legends - 13th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'India Legends', 'South Africa Legends', '2021-03-13 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(42, 'pslt20_2021_g26', 'Multan Sultans vs Islamabad United', 'MS vs IU', 'Multan Sultans vs Islamabad United - 26th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Multan Sultans', 'Islamabad United', '2021-03-13 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(43, 'indwrsaw_2021_one-day_04', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 4th ODI Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 'one-day', NULL, 'India Women', 'South Africa Women', '2021-03-14 03:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(44, 'pslt20_2021_g27', 'Quetta Gladiators vs Peshawar Zalmi', 'QG vs PZ', 'Quetta Gladiators vs Peshawar Zalmi - 27th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Quetta Gladiators', 'Peshawar Zalmi', '2021-03-14 09:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(45, 'rswst20_2020_g14', 'Sri Lanka Legends vs England Legends', 'SLL vs ENGL', 'Sri Lanka Legends vs England Legends - 14th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'Sri Lanka Legends', 'England Legends', '2021-03-14 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(46, 'indeng_2021_t20_02', 'India vs England', 'IND vs ENG', 'India vs England - 2nd T20 Match - India vs England 2021', 'India vs England 2021', 't20', NULL, 'India', 'England', '2021-03-14 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(47, 'wisl_2021_one-day_03', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 3rd ODI Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 'one-day', NULL, 'West Indies', 'Sri Lanka', '2021-03-14 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(48, 'pslt20_2021_g28', 'Lahore Qalandars vs Karachi Kings', 'LQ vs KK', 'Lahore Qalandars vs Karachi Kings - 28th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Lahore Qalandars', 'Karachi Kings', '2021-03-14 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(49, 'rswst20_2020_g15', 'South Africa Legends vs Bangladesh Legends', 'RSAL vs BANL', 'South Africa Legends vs Bangladesh Legends - 15th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'South Africa Legends', 'Bangladesh Legends', '2021-03-15 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(50, 'pslt20_2021_g29', 'Islamabad United vs Peshawar Zalmi', 'IU vs PZ', 'Islamabad United vs Peshawar Zalmi - 29th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Islamabad United', 'Peshawar Zalmi', '2021-03-15 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(51, 'indeng_2021_t20_03', 'India vs England', 'IND vs ENG', 'India vs England - 3rd T20 Match - India vs England 2021', 'India vs England 2021', 't20', NULL, 'India', 'England', '2021-03-16 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(52, 'rswst20_2020_g16', 'England Legends vs West Indies Legends', 'ENGL vs WIL', 'England Legends vs West Indies Legends - 16th Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'England Legends', 'West Indies Legends', '2021-03-16 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(53, 'pslt20_2021_g30', 'Lahore Qalandars vs Multan Sultans', 'LQ vs MS', 'Lahore Qalandars vs Multan Sultans - 30th Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'Lahore Qalandars', 'Multan Sultans', '2021-03-16 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(54, 'indwrsaw_2021_one-day_05', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 5th ODI Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 'one-day', NULL, 'India Women', 'South Africa Women', '2021-03-17 03:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(55, 'afgzim_2021_t20_01', 'Afghanistan vs Zimbabwe', 'AFG vs ZIM', 'Afghanistan vs Zimbabwe - 1st T20 Match - Afghanistan v Zimbabwe in UAE, 2021', 'Afghanistan v Zimbabwe in UAE, 2021', 't20', NULL, 'Afghanistan', 'Zimbabwe', '2021-03-17 10:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(56, 'rswst20_2020_sf1', 'India Legends vs West Indies Legends', 'INDL vs WIL', 'India Legends vs West Indies Legends - 1st Semi Final Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'India Legends', 'West Indies Legends', '2021-03-17 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(57, 'indeng_2021_t20_04', 'India vs England', 'IND vs ENG', 'India vs England - 4th T20 Match - India vs England 2021', 'India vs England 2021', 't20', NULL, 'India', 'England', '2021-03-18 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(58, 'pslt20_2021_q', 'TBC  vs TBC', 'TBC  vs TBC', 'TBC  vs TBC - Qualifier Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'TBC', 'TBC', '2021-03-18 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(59, 'afgzim_2021_t20_02', 'Afghanistan vs Zimbabwe', 'AFG vs ZIM', 'Afghanistan vs Zimbabwe - 2nd T20 Match - Afghanistan v Zimbabwe in UAE, 2021', 'Afghanistan v Zimbabwe in UAE, 2021', 't20', NULL, 'Afghanistan', 'Zimbabwe', '2021-03-19 10:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(60, 'rswst20_2020_sf2', 'Sri Lanka Legends vs South Africa Legends', 'SLL vs RSAL', 'Sri Lanka Legends vs South Africa Legends - 2nd Semi Final Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'Sri Lanka Legends', 'South Africa Legends', '2021-03-19 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(61, 'pslt20_2021_e1', 'TBC vs TBC', 'TBC vs TBC', 'TBC vs TBC - 1st Eliminator Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'TBC', 'TBC', '2021-03-19 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(62, 'nzban_2021_one-day_01', 'New Zealand vs Bangladesh', 'NZ vs BAN', 'New Zealand vs Bangladesh - 1st ODI Match - Bangladesh tour of New Zealand, 2021', 'Bangladesh tour of New Zealand, 2021', 'one-day', NULL, 'New Zealand', 'Bangladesh', '2021-03-19 22:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(63, 'afgzim_2021_t20_03', 'Afghanistan vs Zimbabwe', 'AFG vs ZIM', 'Afghanistan vs Zimbabwe - 3rd T20 Match - Afghanistan v Zimbabwe in UAE, 2021', 'Afghanistan v Zimbabwe in UAE, 2021', 't20', NULL, 'Afghanistan', 'Zimbabwe', '2021-03-20 10:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(64, 'indwrsaw_2021_t20_01', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 1st T20 Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 't20', NULL, 'India Women', 'South Africa Women', '2021-03-20 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(65, 'indeng_2021_t20_05', 'India vs England', 'IND vs ENG', 'India vs England - 5th T20 Match - India vs England 2021', 'India vs England 2021', 't20', NULL, 'India', 'England', '2021-03-20 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(66, 'pslt20_2021_e2', 'TBC vs TBC', 'TBC vs TBC', 'TBC vs TBC - 2nd Eliminator Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'TBC', 'TBC', '2021-03-20 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(67, 'rswst20_2020_final1', 'India Legends vs Sri Lanka Legends', 'INDL vs SLL', 'India Legends vs Sri Lanka Legends - Final Match - Road Safety World Series 2020-21', 'Road Safety World Series 2020-21', 't20', NULL, 'India Legends', 'Sri Lanka Legends', '2021-03-21 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(68, 'indwrsaw_2021_t20_02', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 2nd T20 Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 't20', NULL, 'India Women', 'South Africa Women', '2021-03-21 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(69, 'wisl_2021_test_01', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 1st Test Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 'test', NULL, 'West Indies', 'Sri Lanka', '2021-03-21 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(70, 'pslt20_2021_final', 'TBC vs TBC', 'TBC vs TBC', 'TBC vs TBC - Final Match - Pakistan Super League 2021', 'Pakistan Super League 2021', 't20', NULL, 'TBC', 'TBC', '2021-03-22 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(71, 'nzban_2021_one-day_02', 'New Zealand vs Bangladesh', 'NZ vs BAN', 'New Zealand vs Bangladesh - 2nd ODI Match - Bangladesh tour of New Zealand, 2021', 'Bangladesh tour of New Zealand, 2021', 'one-day', NULL, 'New Zealand', 'Bangladesh', '2021-03-23 01:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(72, 'indeng_2021_one-day_01', 'India vs England', 'IND vs ENG', 'India vs England - 1st ODI Match - India vs England 2021', 'India vs England 2021', 'one-day', NULL, 'India', 'England', '2021-03-23 08:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(73, 'indwrsaw_2021_t20_03', 'India Women vs South Africa Women', 'INDW vs RSAW', 'India Women vs South Africa Women - 3rd T20 Match - India Women vs South Africa Women 2021', 'India Women vs South Africa Women 2021', 't20', NULL, 'India Women', 'South Africa Women', '2021-03-23 13:30:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(74, 'nzban_2021_one-day_03', 'New Zealand vs Bangladesh', 'NZ vs BAN', 'New Zealand vs Bangladesh - 3rd ODI Match - Bangladesh tour of New Zealand, 2021', 'Bangladesh tour of New Zealand, 2021', 'one-day', NULL, 'New Zealand', 'Bangladesh', '2021-03-25 22:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(75, 'indeng_2021_one-day_02', 'India vs England', 'IND vs ENG', 'India vs England - 2nd ODI Match - India vs England 2021', 'India vs England 2021', 'one-day', NULL, 'India', 'England', '2021-03-26 08:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(76, 'nzban_2021_t20_01', 'New Zealand vs Bangladesh', 'NZ vs BAN', 'New Zealand vs Bangladesh - 1st T20 Match - Bangladesh tour of New Zealand, 2021', 'Bangladesh tour of New Zealand, 2021', 't20', NULL, 'New Zealand', 'Bangladesh', '2021-03-28 01:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(77, 'nzwausw_2021_t20_01', 'New Zealand Women vs Australia Women', 'NZW vs AUSW', 'New Zealand Women vs Australia Women - 1st T20 Match - Australia Women tour of New Zealand, 2021', 'Australia Women tour of New Zealand, 2021', 't20', NULL, 'New Zealand Women', 'Australia Women', '2021-03-28 06:10:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(78, 'indeng_2021_one-day_03', 'India vs England', 'IND vs ENG', 'India vs England - 3rd ODI Match - India vs England 2021', 'India vs England 2021', 'one-day', NULL, 'India', 'England', '2021-03-28 08:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(79, 'wisl_2021_test_02', 'West Indies vs Sri Lanka', 'WI vs SL', 'West Indies vs Sri Lanka - 2nd Test Match - Sri Lanka tour of West Indies, 2021', 'Sri Lanka tour of West Indies, 2021', 'test', NULL, 'West Indies', 'Sri Lanka', '2021-03-29 14:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(80, 'nzwausw_2021_t20_02', 'New Zealand Women vs Australia Women', 'NZW vs AUSW', 'New Zealand Women vs Australia Women - 2nd T20 Match - Australia Women tour of New Zealand, 2021', 'Australia Women tour of New Zealand, 2021', 't20', NULL, 'New Zealand Women', 'Australia Women', '2021-03-30 02:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34'),
(81, 'nzban_2021_t20_02', 'New Zealand vs Bangladesh', 'NZ vs BAN', 'New Zealand vs Bangladesh - 2nd T20 Match - Bangladesh tour of New Zealand, 2021', 'Bangladesh tour of New Zealand, 2021', 't20', NULL, 'New Zealand', 'Bangladesh', '2021-03-30 06:00:00', '2021-03-28 06:10:34', '2021-03-28 06:10:34');

-- --------------------------------------------------------

--
-- Table structure for table `PoolRanks`
--

CREATE TABLE `PoolRanks` (
  `id` int(11) NOT NULL,
  `pool_id` int(11) NOT NULL,
  `rank_range` varchar(125) DEFAULT NULL,
  `prize_money` varchar(125) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Pools`
--

CREATE TABLE `Pools` (
  `id` int(11) NOT NULL,
  `match_id` int(11) NOT NULL,
  `user_limit` int(11) DEFAULT NULL,
  `entry_fees` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Referrals`
--

CREATE TABLE `Referrals` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `referral_code` varchar(125) NOT NULL,
  `referrer` varchar(125) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Referrals`
--

INSERT INTO `Referrals` (`id`, `user_id`, `referral_code`, `referrer`, `createdAt`, `updatedAt`) VALUES
(1, 1, 'EZq4D-zrN', '', '2021-03-28 10:36:10', '2021-03-28 10:36:10'),
(2, 2, 'Uc5rTqRwP', '', '2021-03-28 13:37:56', '2021-03-28 13:37:56'),
(3, 3, 'StGgpbGfd', '', '2021-03-28 13:42:38', '2021-03-28 13:42:38'),
(4, 4, '1935x7JIE', 'cKP_Gj878', '2021-03-28 14:22:55', '2021-03-28 14:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `SportsPlayer`
--

CREATE TABLE `SportsPlayer` (
  `id` int(11) NOT NULL,
  `team_id` int(11) NOT NULL,
  `player_name` varchar(125) NOT NULL,
  `category` varchar(125) DEFAULT NULL,
  `point_cost` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Team`
--

CREATE TABLE `Team` (
  `id` int(11) NOT NULL,
  `match_id` int(11) DEFAULT NULL,
  `team_name` varchar(125) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `id` int(11) NOT NULL,
  `username` varchar(125) DEFAULT NULL,
  `email` varchar(125) DEFAULT NULL,
  `salt` varchar(125) DEFAULT NULL,
  `hash` text,
  `mobile` varchar(125) DEFAULT NULL,
  `address1` varchar(125) DEFAULT NULL,
  `address2` varchar(125) DEFAULT NULL,
  `pincode` varchar(125) DEFAULT NULL,
  `city` varchar(125) DEFAULT NULL,
  `country` varchar(125) DEFAULT NULL,
  `aadhar_front` varchar(125) DEFAULT NULL,
  `aadhar_back` varchar(125) DEFAULT NULL,
  `selfie` varchar(125) DEFAULT NULL,
  `pancard` varchar(125) DEFAULT NULL,
  `email_confirmed` tinyint(1) DEFAULT NULL,
  `mobile_confirmed` tinyint(1) DEFAULT NULL,
  `kyc_verified` tinyint(1) DEFAULT NULL,
  `document_id` varchar(125) DEFAULT NULL,
  `email_register_expires` datetime DEFAULT NULL,
  `email_register_token` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`id`, `username`, `email`, `salt`, `hash`, `mobile`, `address1`, `address2`, `pincode`, `city`, `country`, `aadhar_front`, `aadhar_back`, `selfie`, `pancard`, `email_confirmed`, `mobile_confirmed`, `kyc_verified`, `document_id`, `email_register_expires`, `email_register_token`, `createdAt`, `updatedAt`) VALUES
(1, 'afzal', 'afzal@tersesoft.com', '11f9e081b68e8b8309bb12f6648cc01b', '8d77d22e84331b17c7f43c9fd87457ff5df1b2497c80853c024bf4dee58055d9ecd072cf762d209d736d27c19e0e48c4c0af85f369837e83039ea61dfae52b9bffb67c808557dcf3508af3662bc618f053f3e579887635642a9150e5b7c7319f0ce24a350f8090f88a944ff27b8a6cbfe056b352a4e94518ab9753b5d3b3866f78c4aa695c1278523af54081fd73d6a73928774b1bc7c1a7d756e11e551f0a302ebdf5025d22014eeff9fbc41d40944e72ce67fb33bc58e85a519db355d3406974d2b36d44347fac5ebf3e7bdca496e070a68ed0bef5fa3d3f6b7a1670c5c03e6c5f49eb59ddd4cb58da80b9cd4b42f91fd28d39a094e17d8409771ed143f5c2ca063d34f7fd20140b9ce8a789769bfc5ae61f7c84cd87c65132d885f83deda1758af97beb33ad449bbf482a17f7f44b3bd7599a69c1c512840d43ed69505cad19b930b1fe944990bad112d01c9c15e35b9dedaeb04a868164ff470411424f9ff57d7ba824b852b6813c8660195c0480bc4f53eb840e5ace475b9b23a129532c07e6ea1ea2939c6670d28867ee9745118c8cb733197df8beb094ccc81852ca4149bf9e05a9e70218fc437e95cd7c52abcbd7203e2a0f27e42848395dc17f4bb600a7a29afd1657e743867fd8329ba084e4ae917945106a3c4469f892b8f611d122a483d602a042e5b0f96f91d683e855a817fce5f1559bac10fc7ca79d56ab1d', '+91afzal@tersesoft.com', '', '', '', '', 'India', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 10:36:10', '2021-03-28 10:36:10'),
(2, 'Afzal', 'mohd.afzal16@gmail.com', '7fe8b57714f711a710248386362429b9', 'd1fd7713d53edd289a51f0bc42868c2e01d38d4ec2d0bafc43eb9cf8a53d87dc0860c9bb49f42868aa606a8306519ffa90d535d3c7a2b76b7c3e4c64ed737ae8ea4335ed7bd6aee2f8728b1306e2651644b32980698b506539af0a6ae005d0662a801dba3bbd2e96cc19d9d14eb4ecd6aa2ec04d32694ca6bd0ec5655209c60e5b09b545638480043fa2ba3264f47605a55d82e53c688e99cc56c6b83e2cf49547d34fc0b783757f435be0e3597a5f7d2f4dae53237c79ea257e3ff864e7f6f65326740180481361488aa4199b878c964b1115cb5decdc9c11277cb8540237bbfc8d47f0a9e22a16104d3e3c3aeb8c5f7fa86752240dfafd7e9137264f21395aea4f2296a29e87571c8a09d1dddd2db0d2b414778b1b6a725ff0f53a9a1bd7474fab82e75963b5b203e9c75a0332c04195194ed0a55945b02ef6cf562ebc227f04bac6b497f17a36a04e40a9be51e5ac3696343c6a6dc5c4a9524f652e210f04daa922ae43bd6f11686142b16c01a18f899b9af13437ff5eed868812b82bc15c8f57208b1b343a6da13885f1c14eb2f639e5c2048d61c2af4131c2107980a0f1276e43237a2525c4675394739b40ef1ecd3130fc01ffcf019be599b17a8410d57fa7825f9474f938f9cf893f7c3e277b6980f4850c6622d47b2a22e324a64f86c73f99c9237cb62190359d63fbe9f7af6325133fff98ccbea0fd159e1c0d5b75', '+91mohd.afzal16@gmail.com', '', '', '', '', 'India', NULL, NULL, NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 13:37:56', '2021-03-28 13:37:56'),
(3, 'afzal', 'afzallegend@yahoo.com', '5734f3aa4374e9c6473d0aee893ddf61', 'f3b0822900ca7468db77167a223a3497ec71767761eb985449a0e907a4843b7e1d3697dae03eb9cdb4cd51d8fe3c18e68552104f29e59586fbd7f9dc5e2991670a57c531664e53c111b19e58d2d100a8c02155b71054aeae90a3e7eaa17b3413128125c646ce7ac9b704980ddebf459eb530da3f652ecdfbac41bc07c0d7fb31a0465c97e28221efd9d1202b1063a79c899493c99ca9871e3fa81558182c457f7262cec11ee6a5ccf7f526d15bb1003471f5d28e55b09373b5692759c23756ce30d644b3c86a80b8c44cd9dfab20c57ed460a0003e7d9e8f8b2f41f119e8b6e860609044d827cee93cf18c864cee1793de72e9bfb135d36f586c28ef44a7a9c8a05bffc94d999076734035a2f0f7382e59b430a3ba383934f24c600c970c12143fcc11b7360f5390d0a59f47a5371e8299e78dacb274b46b9b5854258246308ed58144bd68d5bd994671ada77521eadcacd80951520da4deb99bf014fa37e56eba599b7a72e41b79b411f07bfc63309f7a37bbef27db0dd3ab01c632b2ad081828c0aa1e6bd9f726255c8b120d8ac65940b35c083f0ab2022ed3e66fde4764c59b8b27328638f4e0f0da60e338e61495b9f0e4faa56e6d41a3cecdf9ee8ae269e71681ff7d0d055c633d1fd3518fd5a55f0a5e0d3638c5f9cd2989c962af00916997e115687783afbf139779b9e7ffbdb0af25a04d53250e3276cf3c1194f8fd', '+91afzallegend@yahoo.com', '', '', '', '', 'India', 'https://crypto-cricket.s3.ap-southeast-1.amazonaws.com/aadhar_front_3', 'https://crypto-cricket.s3.ap-southeast-1.amazonaws.com/aadhar_back_3', NULL, '', 1, 1, NULL, NULL, NULL, NULL, '2021-03-28 13:42:38', '2021-03-28 13:50:24'),
(4, 'vijendra', 'vijendra@tersesoft.com', 'c96e1cfa832c7b2c0b65e8f16be2f6c0', 'e849268eedcbd06d99deb14a3e30ba1be505a3af11a8a7f3d53b4e6af762ef93ca081e0eb7d0dad4ed658292d9fceec5242c330b10f40d9b213fbf11f9c195e4363905dc7fd162acd4599560dc0dfffc11b753584d401a17842679691339c12838cecfb12d8d2b28e28f53fb0f097af98b60155cc9dcab975a2d3ed2af017e9a2d98f7e4b0b4c7e16fa245bda1dd5d981934792093cfe42855ac23ad48a4b7068b46c0db2abcb66fdc8c786bf2f8d00dc0a4b93e7280612e7c6170c06dbd988eb5ebd0f57ad65b819335986f40fc826f8dd3b5ec6d251fa2848405397e9eb562a75e0de74d4dbcb4ac0d8202e569a15656964d95b008e231bc2e4db7ad02e33c6c04da04d2786ed3f51f735b21cdaccac12343fb29a904ae1dab64f2df70a2e4ac53f432ad87ecdd0d9e5a3ed5a3029a446b007d1d0bc90f3f66d3ecdc1e666af7a36203767a554fe6d04a80a3652f0debc4249b5fff07a3344902d78604febd9da6cab9c304fcb915d962eb5feb8eb2618d991b84a6b89907f9cd53d3a7cd1d8ef050ad14b2ddd66c035a271928815aa90051fe6dd5a07f388da1584e7d8a122694543c568a269b62753e200fccfe9def82131b5729b3e0168ac60ee9330158985abbb065e8a1997f7db3fff83fb73ce6d1645e508e8edbf57d092f4f8402e8e4c8562ff1aeaf7a6ceac12b28e62887d4cd8d0c155ba2bfbaaffeb2d42b1af5', '+918668378456', NULL, NULL, NULL, NULL, 'Maharashtra', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2021-03-28 14:22:55', '2021-03-28 14:22:55');

-- --------------------------------------------------------

--
-- Table structure for table `UsersTeam`
--

CREATE TABLE `UsersTeam` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `pool_id` int(11) DEFAULT NULL,
  `players_ids` varchar(255) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Wallet`
--

CREATE TABLE `Wallet` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `wallet_id` varchar(125) NOT NULL,
  `public_key` varchar(125) NOT NULL,
  `hbar_balance` int(11) DEFAULT NULL,
  `stoken_balance` int(11) DEFAULT NULL,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Wallet`
--

INSERT INTO `Wallet` (`id`, `user_id`, `wallet_id`, `public_key`, `hbar_balance`, `stoken_balance`, `createdAt`, `updatedAt`) VALUES
(1, 1, '0.0.466503', '302a300506032b657003210047d23b3ae90e344e47a77bd2e50f87a97e07a6f2946069008ef6866be69bc07c', NULL, NULL, '2021-03-28 10:36:12', '2021-03-28 10:36:12'),
(2, 2, '0.0.466981', '302a300506032b65700321001262c4bdb519cabcc227d4713cdbfae31af393277318b5ea03d21146b1de9ee2', NULL, NULL, '2021-03-28 13:37:58', '2021-03-28 13:37:58'),
(3, 3, '0.0.466983', '302a300506032b657003210003e65f7a32f7ca83aba18ba18c57758a334d43e39f280295ef76b91a189bde94', NULL, NULL, '2021-03-28 13:42:40', '2021-03-28 13:42:40'),
(4, 4, '0.0.466997', '302a300506032b6570032100e4ac8c5665bba27b826451211270ee76f1cc60491ec105c1b8d5365dfa6b6fb5', NULL, NULL, '2021-03-28 14:22:57', '2021-03-28 14:22:57');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Matches`
--
ALTER TABLE `Matches`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `PoolRanks`
--
ALTER TABLE `PoolRanks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Pools`
--
ALTER TABLE `Pools`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Referrals`
--
ALTER TABLE `Referrals`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `SportsPlayer`
--
ALTER TABLE `SportsPlayer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Team`
--
ALTER TABLE `Team`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `UsersTeam`
--
ALTER TABLE `UsersTeam`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `Wallet`
--
ALTER TABLE `Wallet`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `Matches`
--
ALTER TABLE `Matches`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `PoolRanks`
--
ALTER TABLE `PoolRanks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Pools`
--
ALTER TABLE `Pools`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Referrals`
--
ALTER TABLE `Referrals`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `SportsPlayer`
--
ALTER TABLE `SportsPlayer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Team`
--
ALTER TABLE `Team`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `User`
--
ALTER TABLE `User`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `UsersTeam`
--
ALTER TABLE `UsersTeam`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Wallet`
--
ALTER TABLE `Wallet`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

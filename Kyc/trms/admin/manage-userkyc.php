<?php
session_start();
error_reporting(0);
include('includes/dbconnection.php');
if (strlen($_SESSION['trmsaid']==0)) {
  header('location:logout.php');
  } else{



  ?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    
    <title>KYC Manage Users</title>
    
    <link rel="apple-touch-icon" href="apple-icon.png">
  


    <link rel="stylesheet" href="vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="vendors/font-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="vendors/themify-icons/css/themify-icons.css">
    <link rel="stylesheet" href="vendors/flag-icon-css/css/flag-icon.min.css">
    <link rel="stylesheet" href="vendors/selectFX/css/cs-skin-elastic.css">

    <link rel="stylesheet" href="assets/css/style.css">

    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,600,700,800' rel='stylesheet' type='text/css'>



</head>

<body>
    <!-- Left Panel -->

    <?php include_once('includes/sidebar.php');?>

    <div id="right-panel" class="right-panel">

        <!-- Header-->
        <?php include_once('includes/header.php');?>
        <div class="breadcrumbs">
            <div class="col-sm-4">
                <div class="page-header float-left">
                    <div class="page-title">
                        <h1>Manage Users</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-8">
                <div class="page-header float-right">
                    <div class="page-title">
                        <ol class="breadcrumb text-right">
                            <li><a href="dashboard.php">Dashboard</a></li>
                            <li><a href="manage-subjects.php">Manage Users</a></li>
                            <li class="active">Manage Users</li>
                        </ol>
                    </div>
                </div>
            </div>
        </div>

        <div class="content mt-3">
            <div class="animated fadeIn">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-header">
                                <strong class="card-title">Manage Users</strong>
                            </div>
                            <div class="card-body">
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <tr>
                  <th>S.NO</th>           
                  <th>User Name</th>
                    <th>Aadhar front</th> 
                     <th>Aadhar back</th>   
                     <th>Selfie</th> 
                     <th>Pancard</th>       
                   <th>Action</th>
                </tr>
                                        </tr>
                                        </thead>
                                    <?php
$sql="SELECT * from User";
$query = $dbh -> prepare($sql);
$query->execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
$cnt=1;
if($query->rowCount() > 0)
{
foreach($results as $row)
{  
$id = $row->id;             ?>   
              
             
                <tr> 
                  <td><?php echo htmlentities($cnt);?></td>           
                  <td><?php  echo htmlentities($row->username);?></td>
                  <td> <a href="<?php echo htmlentities($row->aadhar_front);?>" target="_blank"> 
<img width="150" height="150" border="0" align="center"  src="<?php echo htmlentities($row->aadhar_front);?>"/>
</a></td>
                  <td> <a href="<?php echo htmlentities($row->aadhar_front);?>" target="_blank"> 
<img width="150" height="150" border="0" align="center"  src="<?php echo htmlentities($row->aadhar_front);?>"/>
</a></td>
                  <td><a href="<?php echo htmlentities($row->selfie);?>" target="_blank"> 
<img width="150" height="150" border="0" align="center"  src="<?php echo htmlentities($row->selfie);?>"/>
</a></td>
                  <td><a href="<?php echo htmlentities($row->pancard);?>" target="_blank"> 
<img width="150" height="150" border="0" align="center"  src="<?php echo htmlentities($row->pancard);?>"/>
</a></td>
                 <td><?php 
                          if($row->kyc_verified==1){
                            ?>
<label style="color:green;" >Verified</label>
                            <?php 
                          }else{
                            ?>
<button class="update-status" style="background-color: #4CAF50; color: white;font-size: 15px;cursor: pointer;" data-id="<?=$id?>">Make verify </button>
                            <?php } ?></td>
                  
                </tr>
               <?php $cnt=$cnt+1;}} ?>   

                                </table>
                            </div>
                        </div>
                    </div>



                </div>
            </div><!-- .animated -->
        </div><!-- .content -->


    </div><!-- /#right-panel -->

    <!-- Right Panel -->


    <script src="vendors/jquery/dist/jquery.min.js"></script>
    <script src="vendors/popper.js/dist/umd/popper.min.js"></script>
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="assets/js/main.js"></script>

<script type="text/javascript">
  (function($){
    $(document).ready(function(){
    $('.update-status').on('click', function(){
      let id = $(this).data('id');
      let elem = this;
      $.ajax({
        type: 'POST',
        url: 'update.php',
        data: {
          id : id
        }
      }).done(function() {
        $(elem).parent().html('<label style="color:green;" >Verified</label>');
      })
    })
  })
  })(jQuery)
  

</script>
</body>

</html>
<?php }  ?>
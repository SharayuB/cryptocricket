<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Login extends CI_Controller{


	public function __construct(){
		parent::__construct();

		$this->load->helper('url');
	}

	public function index(){
		$operation = $this->input->get('op');
		$data['status'] = '';
		if($operation == 1){
			$data['status'] = 'Username or password is invalid. Please try again.';
		}
		$this->load->view('login',$data);
	}

}


?>
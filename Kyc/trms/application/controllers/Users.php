<?php
/**
 *
 * Aurovine Users list
 *
 */

defined('BASEPATH') or die('No direct script asscess is allowed');


class Users extends CI_Controller {


public function __construct(){
parent::__construct();
$this->load->helper('url');
$this->load->model('user_model','users');
}

public function users_list(){
$this->load->view('users_list');
}



function webhits(){

$fdate = $this->input->post('post_at');
   $tdate = $this->input->post('post_at_to_date');
      $usersData = $this->users->get_user_datewise($fdate,$tdate);
//echo json_encode($list);
$data['users'] = $usersData;    
        $this->load->view('wehhits_list',$data);

}



// Export data in CSV format
  function exportCSV(){

  //print_r($_POST);
   $fdate = $this->input->post('hpost_at');
   $tdate = $this->input->post('hpost_at_to_date');
      $usersData = $this->users->get_user_datewise($fdate,$tdate);

  $filename = 'users_'.date('Ymd').'.csv';
  header("Content-Description: File Transfer");
  header("Content-Disposition: attachment; filename=$filename");
  header("Content-Type: application/csv; ");
   
   // get data
 

   // file creation
   $file = fopen('php://output', 'w');
 
   $header = array("id","username","email","paypal_email","createdAt");
   fputcsv($file, $header);
   foreach ($usersData as $key=>$line){
     fputcsv($file,$line);
   }
   fclose($file);
   exit;
  }



}


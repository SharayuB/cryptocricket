<?php include "header.php" ?>
<!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Users</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Users</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <?php 
    // $users = $data->SUCC;
    ?>

    <!-- Main content -->
    <section class="content">
     
      

      <div class="row">
        <div class="col-12">
          <div class="card">
            <div class="card-header">
              <h3 class="card-title">Artists subscription list</h3>
                            <!-- Trigger the modal with a button -->
<button type="button" class="btn btn-info btn-md float-right" data-toggle="modal" data-target="#myModal">Add User</button>	
            </div>
            <!-- /.card-header -->

            <p>From: <input class="datepicker" id="dateFrom" type="text"> To: <input class="datepicker" id="dateTo" type="text"><button  class="buttApply">APPLY</button></p>


            <div class="card-body">
              <table id="example2" class="table table-bordered table-hover" style="width:100%">
                <thead>
                <tr>
                  <th>Id</th>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Account Type</th>
                  <th>Created At</th>
                </tr>
                </thead>
                <tbody> 
                  <?php
                    
                   ?>
                </tbody>
                <tfoot>
                <tr>
                  <th>Id</th>
                  <th>Photo</th>
                  <th>Name</th>
                  <th>Email</th>
                  <th>Account Type</th>
                  <th>Created At</th>
                </tr>
                </tfoot>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <!-- /.card -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
      <div class="row" style="background:white;padding:10px">
        <div class="w-100 text-center">
          <div class="loader" style="margin:0 auto;display:none;"></div>
          <div class="loader-txt" >Loading data Please wait...</div>
        </div>
      </div>
    </section>
    <!-- /.content -->


  	<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add User </h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>
      <div class="modal-body">
        <div class="col-md-12">
            <!-- general form elements -->
              <!-- form start -->
              <form role="form">
                	<div class="row">
                 	<div class="col-md-12 d-flex">
                      <div class="col-md-2">
                      	<div class="form-group">
                          <label for="Name">Name:</label>
                      	</div>
                      </div>
                      <div class="col-md-10">
                         <input type="name" class="form-control" id="name" placeholder="Enter Name">
                      </div>
                    </div>
                    <div class="col-md-12  d-flex">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="Email">Email:</label>
                        </div>
                      </div>
                      <div class="col-md-10">
                         <input type="email" class="form-control" id="email" placeholder="Enter Email">
                      </div>
                    </div>
                    <div class="col-md-12  d-flex">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="age">Age:</label>
                        </div>
                      </div>
                      <div class="col-md-10">
                         <input type="age" class="form-control" id="age" placeholder="Enter age">
                      </div>
                    </div>
                    <div class="col-md-12  d-flex">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="Phone">Phone:</label>
                        </div>
                      </div>
                      <div class="col-md-10">
                         <input type="phone" class="form-control" id="phone" placeholder="Enter Phone">
                      </div>
                    </div>
                    <div class="col-md-12  d-flex">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="country">Country:</label>
                        </div>
                      </div>
                      <div class="col-md-10">
                         <input type="country" class="form-control" id="country" placeholder="Enter Country">
                      </div>
                    </div>

                    <div class="col-md-12  d-flex">
                      <div class="col-md-2">
                        <div class="form-group">
                          <label for="city">City:</label>
                        </div>
                      </div>
                      <div class="col-md-10">
                         <input type="city" class="form-control" id="country" placeholder="Enter City">
                      </div>
                    </div>
                    
            </form>
        </div>
      </div>
      <div class="modal-footer">
      	<button type="submit" class="btn btn-primary">Submit</button>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>

<?php include "footer.php"; ?>
<script src="js/jquery.js" type="text/javascript"></script>
  <script src="js/jquery.dataTables.js" type="text/javascript"></script>
    
<script >
 $.ajax({
      url: '<?= API_URL ?>landing/get_all_users',
      cache: false,
      success: function(html){
        console.log(html)
        var data = {
          "data": [
          ]
        }
        $.map( html.SUCC, function( h, i ) {
          // Do something
          
          var str2 = "aurovine-photos";
          if(h.photo == null || h.photo == 'null' ){
            h.photo = '<img src="<?= base_url();?>assets/images/no-avatar.png" width="50px"/>';
          }else{
            h.photo = '<img src="'+h.photo+'" width="50px"/>';
          }

          if(h.photo.indexOf(str2) != -1){
            console.log(h.photo);
          }else{
            h.photo = '<img src="<?= base_url();?>assets/images/no_image.png" width="50px"/>';
          }

          let a = [h.id, h.photo, h.username, h.email, h.account_type, h.createdAt]
          data.data.push(a)
        });
        console.log('all data');
        console.log(data)
       $(document).ready(function() {
       $('#example2').DataTable();
});
      },
      beforeSend: function(){
        $(".loader").show();
        // $("#example2").hide();
      },
      complete: function(){
        console.log('Completed')
        $('#loading-image').hide();
        $(".loader").hide();
        $(".loader-txt").hide();
      }
    });
  
</script>
 
<?php include "end.php";?>

  <link rel="stylesheet" type="text/css" href="http://jqueryui.com/themes/base/jquery.ui.all.css">
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script>
      <link rel="stylesheet" type="text/css" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/themes/mint-choc/jquery-ui.css">
      <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.12/jquery-ui.min.js"></script>